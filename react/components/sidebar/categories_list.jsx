import React, { Component } from 'react'
import { Typography, List, ListItem, ListItemText, Collapse } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import { ExpandLess, ExpandMore } from '@material-ui/icons';
import { productsApi } from '../../api'
import { connect } from "react-redux";
import { bindActionCreators } from 'redux'
import { productsActions } from '../../store/actions'

const styles = theme => ({
    subtitle: {
        padding: "5px 20px",
        fontWeight: 500,
        '& svg': {
            verticalAlign: "middle"
        }
    },
    cat_li: {
        padding: "0 0 0 10px"
    }
})

class CategoriesList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            categoriesTabOpened: false,
            selectedCategory: -1,
            openedCategories: {}
        }
    }
    componentWillMount() {

    }
    selectCategory = (id) => {
        const { setProducts } = this.props
        productsApi.getProductsByCategory({ id }, (error, data) => {
            if (error) return console.error("getProductsByCategory-Error: ", error)
            console.log("getProductsByCategory-Data: ", data)
            this.setState({ selectedCat: id })
            setProducts({
                list: data,
                totalCount: data.length,
            })
        })
    }

    viewCategory = (category) => {
        const { classes } = this.props;
        const { openedCategories } = this.state;

        return (<React.Fragment key={category.id}>
            <ListItem button onClick={(e) => this.switchTabState(category.id)}>
                <ListItemText primary={category.title} />
                {openedCategories[category.id] ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={openedCategories[category.id]} timeout="auto" unmountOnExit>
                <List component="div" disablePadding className={classes.cat_li}>
                    {category.categories.map(item => this.viewCategory(item))}
                </List>
            </Collapse>
        </React.Fragment>
        )
    }
    switchTabState(id) {
        const { openedCategories } = this.state;
        openedCategories[id] = openedCategories[id] ? false : true
        this.setState({ openedCategories })
        this.selectCategory(id)
    }

    render() {
        const { classes, list } = this.props;
        const { categoriesTabOpened } = this.state;
        return (
            <div>
                <Typography
                    variant="overline"
                    className={classes.subtitle}
                    onClick={(e) => this.setState({ categoriesTabOpened: !categoriesTabOpened })}
                >
                    Categories {categoriesTabOpened ? <ExpandLess /> : <ExpandMore />}
                </Typography>
                <Collapse in={categoriesTabOpened} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding className={classes.cat_li}>
                        {list.map(item => this.viewCategory(item))}
                    </List>
                </Collapse>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    products: state.products
})

const mapDispatchToProps = dispatch => bindActionCreators(
    {
        ...productsActions
    }, dispatch)

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(CategoriesList))