const path = require("path");
const webpack = require("webpack");

module.exports = {
  entry: "./react/index.js",
  mode: "development",
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules|bower_components)/,
        loader: "babel-loader",
        options: { presets: ["@babel/env"] }
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"]
      },
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.(png|jpg|gif)$/i,
          use: [
            {
              loader: 'url-loader',
              options: {}
            }
          ]
      }
    ]
  },
  resolve: {
    extensions: ["*", ".js", ".jsx"],
    alias: {
        Components: path.resolve(__dirname, 'react/components/'),
        Pages: path.resolve(__dirname, 'react/pages/'),
        Store: path.resolve(__dirname, 'react/store/'),
        Config: path.resolve(__dirname, 'react/config.js')
    }
  },
  output: {
    path: path.resolve(__dirname, "web/js"),
    publicPath: "http://localhost:3000/js/",
    filename: "app.js"
  },
  devServer: {
    contentBase: path.join(__dirname, "web/"),
    headers: { 'Access-Control-Allow-Origin': '*' },
    port: 3000,
    hotOnly: true
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ]
};
