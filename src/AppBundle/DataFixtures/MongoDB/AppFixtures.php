<?php

namespace AppBundle\DataFixtures\MongoDB;

use AppBundle\Document\Product;
use AppBundle\Document\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $categories = [];

        for ($i = 0; $i < 3; ++$i) {
            $category = new Category();
            $category->setTitle('category '.$i);
            $manager->persist($category);

            for ($j = 0; $j < 3; ++$j) {
                $subcategory = new Category();
                $subcategory->setTitle('subcategory '.$i.$j);
                $subcategory->setParent($category);
                $manager->persist($subcategory);

                for ($k = 0; $k < 3; ++$k) {
                    $subsubcategory = new Category();
                    $subsubcategory->setTitle('subsubcategory '.$i.$j.$k);
                    $subsubcategory->setParent($subcategory);
                    $manager->persist($subsubcategory);

                    $categories[] = $subsubcategory;
                }
                $categories[] = $subcategory;
            }
            $categories[] = $category;
        }

//        for ($i = 0; $i < 100; ++$i) {
//            $product = new Product();
//            $product->setName('product '.$i);
//            $product->setPrice(mt_rand(10, 100));
//            $product->setAvailable(true);
//            $product->addCategory($categories[mt_rand(0, count($categories) - 1)]);
//            $manager->persist($product);
//        }

        $manager->flush();
    }
}
