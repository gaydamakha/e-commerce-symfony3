<?php

namespace AppBundle\Repository;

use AppBundle\Document\Category;

use Gedmo\Tree\Document\MongoDB\Repository\MaterializedPathRepository;

class CategoryRepository extends MaterializedPathRepository
{
    /**
     * @return Category[] Returns an array of Category objects
     * with all their childrens in 'categories' field
     */
    public function getChildrenHierarchy(Category $category)
    {
        $tree = array_map(function($root){
            return [
                'id' => $root->getId(),
                'title' => $root->getTitle(),
                'categories' => $this->getChildrenHierarchy($root)
            ];
        }, $this->getChildren($category, true, 
                                        ['childSort' => 'title']
                                    ));
        return $tree;
    }

//    /**
//     * @return Category Returns a Category object
//     */
//    public function findById($id)
//    {
//        $qb = $this->getQueryBuilder();
//        $category = $qb->select('c')
//            ->from(Category::class, 'c')
//            ->where('c.id = :id')
//            ->setParameter('id', (int)$id)
//            ->getQuery()
//            ->getSingleResult();
//        return $category;
//    }
}