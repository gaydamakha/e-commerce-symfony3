<?php

namespace AppBundle\Controller;

use AppBundle\Document\Category;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Cart controller.
 *
 * @Route("/api", name="api")
 */
class CategoryController extends FOSRestController
{
    /**
     * List all the Categories.
     *
     * @Rest\Get("/categories", name="categories")
     */
    public function getCategoriesAction()
    {
        $roots = $this->get('doctrine_mongodb')
            ->getManager()
            ->getRepository(Category::class)
            ->getRootNodes('title', 'asc');

        $response = array_map(function ($root) {
            $rp = $this->getDoctrine()
                ->getRepository(Category::class);

            $rp->setChildrenIndex('categories');

            return [
                'id' => $root->getId(),
                'title' => $root->getTitle(),
                'categories' => $rp->getChildrenHierarchy($root),
            ];
        }, $roots);

        return $this->json($response);
    }
}
